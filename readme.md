Create a virtual env with:

    virtualenv -p python3.7 venv
    
Run it with

    source venv/bin/activate
   
Install dependencies with

    pip install -r requirements.txt

Create sqlite db

    touch database.db
    
Install Spacy model

    python -m spacy download en